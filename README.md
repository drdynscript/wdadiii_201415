Cursus WDADIII
=======================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Documenten
---------------

* [Inleiding & Briefings](https://bitbucket.org/drdynscript/wdadiii_201415/src/fbbc001d564f60909d7fd4814e7b452bfbedd613/wdadiii_2014-15.md?at=master "Inleiding WDAD-III")
* [Markdown](https://bitbucket.org/drdynscript/wdadiii_201415/src/22d003ef21a23344c01773d24930803f562f7818/markdown.md?at=master "Markdown Syntax")
* [Applicaties](https://bitbucket.org/drdynscript/wdadiii_201415/src/22d003ef21a23344c01773d24930803f562f7818/applicaties.md?at=master "Applicaties")
* [UniversityCMS Course](https://bitbucket.org/drdynscript/wdadiii_201415/src/c360d8892cf8a2052775db887b55b0df4178058f/wdadiii_universitycms.md?at=master "UniversityCMS Course")
* [UniversityCMS Data Patterns](https://bitbucket.org/drdynscript/wdadiii_201415/src/fbbc001d564f60909d7fd4814e7b452bfbedd613/Data.Pattern.zip?at=master "UnitOfWork & Repository Pattern Bibliotheek")
* [UniversityCMS Project ZIP](https://bitbucket.org/drdynscript/wdadiii_201415/src/c360d8892cf8a2052775db887b55b0df4178058f/UniversityCMS.zip?at=master "UniversityCMS Project ZIP")


Downloads
-------------

- Je kan het wolkje in BitBucket aanklikken om al het cursusmateriaal te downloaden
- Je kan ook eerst het cursusmateriaal lokaal klonen: `git clone https://drdynscript@bitbucket.org/drdynscript/wdadiii_201415.git "naamdirectory"`
- Daarna ga je de wijzigingen lokaal binnentrekken: `git pull -u origin master`. Je moet dan wel in de aangemaakte folder "naamdirectory" zitten!

Makers
--------
**Philippe De Pauw - Waterschoot**

* <http://twitter.com/drdynscript>
* <http://bitbucket.org/drdynscript>
* <https://www.linkedin.com/pub/philippe-de-pauw/6/a33/5a5>
* <philippe.depauw@arteveldehs.be>

Arteveldehogeschool
-------------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>


Copyright and license
--------------------------

Code en documentatie copyright 2003-2015 Arteveldehogeschool | Opleiding Grafische en Digitale Media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de   Creative Commons licentie.