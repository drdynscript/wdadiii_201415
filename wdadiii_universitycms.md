﻿Creating UniversityCMS in ASP.NET MVC5
==================================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Om ASP.NET applicaties aan te maken, zoals Web Applicaties via MVC5 en Web API's, moeten we een doordachte structuur implementeren met de gedachte dat we bepaalde **onderdelen** kunnen **hergebruiken** in andere projecten en projecttypen. Vandaar dat we meestal starten met een **"Blank Solution"** of lege oplossing.

> File \> New Project \> Other Project Types \> Visual Studio Solutions

![enter image description here](https://lh3.googleusercontent.com/-Gs3bOsVEn5o/VCRP8-LbdoI/AAAAAAAAAXU/67W6bX965Ec/s0/blanksolution.PNG "blanksolution.PNG")

We geven een nuttige naam aan deze "Solution". Deze naam moet een beknopte omschrijven geven over het te realiseren project, uiteraard zonder spaties! Vermits we een CMS zullen schrijven voor een Universiteit of Hogeschool, volstaat de naam **"UniversityCMS"**. De locatie van deze "Solution" wordt best bewaard op een dataschijf, dus niet in jouw profiel of op het bureaublad.

Aan deze oplossing voegen we voorlopig een viertal projecten toe:

- **UniversityCMS.Data**  
*Klassenbibliotheek* die alle database acties bevat, beter gekend als CRUD in een **data-layer**.
- **UniversityCMS.Data.Test**
*Unit Test Project* om het voorgaande project uitgebreid te testen.
- **UniversityCMS.Model**
*Klassenbibliotheek* die alle Modellen bevat. Een Model wordt later gekoppeld aan een corresponderende tabel in de databank.
- **UniversityCMS.Web**
*Web Applicatie Project* van het type *MVC*. Dit project is uitvoerbaar en bevat de frontoffice en backoffice, kortom de User Interface. Om dit project als uitvoerbaar in te stellen doen we het volgende:

	> Rechter muistoets op dit project\> Set As Startup Project


![Structuur Solution in Visual Studio 2013](https://lh5.googleusercontent.com/-jEizSkOsSao/VCUat28UQFI/AAAAAAAAAXo/X6Q3BGoI20c/s0/Solution_structure.PNG "Solution_structure.PNG")

Later zullen we nog een aantal andere projecttypen toevoegen, zoals: een Web API, AngularJS SPA, ... waarbij we de modellen en de datalaag steeds zullen hergebruiken. We kunnen eveneens een extra project toevoegen (klassenbibliotheek) die dienst zal doen als **BLL** of **Service-laag**. Deze laatste laag bevat **logica** om bepaalde acties te ondernemen. Boven de vermelde **data-layer** gaan we ook extra **middleware** schrijven om vooral deze datalaag te beschermen volgens een paar gekende patronen.

##Onderdelen van de "Solution"

###UniversityCMS.Data

Dit project doet dienst als datalaag voor ganse "Solution". Deze datalaag bevat CRUD-acties toepasbaar op één of meerdere databanken. De klasse die verantwoordelijk is voor deze databaseacties is de **DbContext** klasse. In deze klasse mappen we o.a. de modellen op de corresponderende tabellen uit de databank. Een model is ook gekend als een **Entity**. Via het **Entity Framework** kunnen we deze mapping realiseren. Zo'n mapper is gekend als een ORM.

> Rechter muistoets op Solution \> Add New Project \> Visual C# \> Class Library

```
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversityCMS.Model;

namespace UniversityCMS.Data.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        #region Properties
        
        #endregion

        #region Constructor
        public ApplicationDbContext():base("DefaultConnection"){}
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
                throw new ArgumentNullException("modelBuilder");

            base.OnModelCreating(modelBuilder);
        }
        #endregion

        #region Public Methods
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
        #endregion
    }
}
```

Aan het **UniversityCMS.Data** project voegen we een nieuwe folder **persistence** toe. In deze folder voegen we o.a. ons eigen "custom" `DbContext` klasse toe, genaamd `ApplicationDbContext`. Deze klasse erft over van de `DbContext` klasse. Onze **context**-klasse bevat voorlopig een *Constructor* en de overgeërfde methoden `OnModelCreating`, `SaveChanges` en `SaveChangesAsync`. Bij de *Constructor* spreken we de constructor uit de basisklasse aan met de *naam van de connectie-string* als argument, in dit geval `DefaultConnection`. In de methode `OnModelCreating` gaan we eerst na of de *mapper* beschikbaar is en spreken we vervolgens de `OnModelCreating` methode aan uit de basisklasse. Deze methode zal vervolgens de mapping bevatten van alle *Modellen* op de corresponderende *Tabellen* via de **Fluent API** syntax. Een referentie van het   **UniversityCMS.Model** project moet toegevoegd worden aan de **references folder** van het **UniversityCMS.Data** project, zodat de naamruimten van het project **UniversityCMS.Model** herkend worden. We vergeten ook niet het **Entity Framework 6+** toe te voegen via de **NuGet Package Manager**.

De migrations, bij de **Models-first** aanpak, worden toegevoegd in de folder **Migrations**.


###UniversityCMS.Model

In applicaties bevat een model toegankelijke eigenschappen die we via een interface kunnen opvullen met inhoud. Deze interface kan een webapplicatie, Web API, Unit Test Project (liefst niet want eigenlijk mag in een Unit Test geen enkele echte CRUD-operatie uitgevoerd worden), ... zijn. Om een goede structuur aan het houden binnen de "Solution", maken we een apart project aan van het type **Class Library** met de naam **UniversityCMS.Model**. Dit project bevat alle modellen die behoren tot deze "Solution". Dit project kan daarnaast ook **View Models** bevatten. Deze modellen zijn complex en kan een verzameling van andere modellen bevatten. **View Models** brengen we, indien van toepassing, onder een aparte folder genaamd **ViewModel**.

> Rechter muistoets op Solution \> Add New Project \> Visual C# \> Class Library

###UniversityCMS.Data.Test

Dit project doet dienst als Unit Test project voor de datalaag. Het doel van dit project is om alle CRUD-operaties te testen bovenop de databank. 

> Rechter muistoets op Solution \> Add New Project \> Visual C# \> Test \> Unit Test Project

Een referentie van de projecten**UniversityCMS.Model** en **UniversityCMS.Data**  moet toegevoegd worden aan de **references folder** van het **UniversityCMS.Data.Test** project, zodat de naamruimten van deze projecten herkend worden. We vergeten ook niet het **Entity Framework 6+** toe te voegen via de **NuGet Package Manager**.

In dit projecten maken we alvast één klasse aan, namelijk `PersistenceTest`. In deze klasse zullen we alle CRUD-operaties, die via de `ApplicationDbContext` klasse uitgevoerd worden, testen.

```
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityCMS.Data.Persistence;
using UniversityCMS.Model;

namespace UniversityCMS.Data.Test
{
    [TestClass]
    public class PersistenceTest
    {
        #region VARIABLES
        ApplicationDbContext _context = new ApplicationDbContext();
        #endregion

        [TestMethod]
        public void Create_Something()
        {
            
        }
    }
}
```

Boven de klassendefinitie  in de klasse  `PersistenceTest` definiëren we de **data-annotatie** `[TextClass]` Deze annotatie is noodzakelijk om deze klasse aan te duiden als zijnde een Unit Test klasse. De klasse moet ook een verwijzing bevatten naar één of meerdere `DbContext`-klassen. In dit geval plaatsen we een nieuwe instantie van de klasse `ApplicationDbContext` in de variabele `_context`. Via deze variabele kunnen we dan alle database acties uitvoeren.

Een testmethode is een methode die **data-annotatie** `[TextMethod]` bevat en waarin we een bepaalde functionaliteit uittesten. De naam van de methode geeft altijd een hint over de te testen functionaliteit.

De **Test explorer** is te bereiken via het hoofdmenu:

> Test \> Windows \> Test Explorer

Voor alle mogelijke **CRUD**-operaties is het aangewezen om de corresponderen **Unit Test Methoden** te beschrijven, bijvoorbeeld:

- `Create_Tag()`  
Aanmaak van een Tag.
- `Get_Tag()`  
Ophalen van een specifieke Tag.
- `Update_Tag()`  
Wijzigen van een Tag.
- `Delete_Tag()`  
Effectief verwijderen van een Tag.
- `VirtualDelete_Tag()`  
Virtueel- of **"softdeleten"** van een Tag.
- `Add_Tag_To_Post()`  
Toevoegen van een bestaande tag aan een post.
- `Remove_Tag_From_Post()`  
Verwijderen van een bestaande tag uit een post.

Eventueel aangevuld met de volgende methoden (in dit specifiek geval):

- `Add_Tags_To_Post()`  
Toevoegen van een bestaande tags aan een post.
- `Remove_Tags_From_Post()`  
Verwijderen van bestaande tags uit een post.

###UniversityCMS.Data.Web

In deze "Solution" gaan we een **Web Application Project** toevoegen van het type **MVC**. Een **controller** bevat **actiemethoden** die aangesproken zullen worden door een bepaalde **route** in te geven in de URL van het browservenster. Een actiemethode bevat meestal een verzameling van acties die al dan niet in sequentie afgehandeld moeten worden. Er worden o.a. databaseacties uitgevoerd om informatie te vergaren, aan te maken, te wijzigen, te verwijderen of virtueel te verwijderen. Het resultaat van deze acties worden naar de corresponderende **view** gestuurd.

Informatie die vergaard werd via acties worden visueel voorgesteld via een **view**. Een view bouwt dus een interface op waarmee een bevoegde gebruiker informatie kan raadplegen en eventueel kan manipuleren. Deze informatie wordt gestopt in een zogenaamd *mandje*. In Web Application projecten, kan zo'n mandje een: *ViewBag*, *ViewData*, *TempData*, *Model*, *Collectie van dezelfde Modellen*, een *ViewModel* of een *combinatie van allen bevatten*.  

> Rechter muistoets op Solution \> Add New Project \> Visual C# \> Web \> ASP.NET Web Application

De output van een actiemethode in  een "Web API" project kan: JSON, BSON, XML, ... zijn.


###Connectionstring

In een connectionstring definiëren we de connectie met de databank en vermelden we ook de cliënt die wordt gebruikt om te connecteren met deze databank. Op de website [connectionstring.com](http://www.connectionstrings.com "Website connectionstrings.com") staat een mooi overzicht van alle mogelijke connecties naar verschillende databank-typen. 

In onze "Solution" zullen we voorlopig werken met een lokaal SQL-bestand. Opgelet dit is eigenlijk "not-done". Omwille van installatieproblemen zullen we voorlopig deze werkwijze hanteren.

```xml
<connectionStrings>
    <add name="DefaultConnection" connectionString="Data Source=(LocalDB)\v11.0;AttachDbFilename=Z:\Opleiding\201415\WDAD-III\UniversityCMS\UniversityCMS.Web\App_Data\universitycms.mdf;Integrated Security=True;Initial Catalog=DefaultConnection"
      providerName="System.Data.SqlClient" />
  </connectionStrings>
```

Omdat we werken met een SQL-databank zal de cliënt van het type `System.Data.SqlClient` zijn. We werken ook voorlopig met een absoluut-pad, omdat we deze connectie ook moeten instellen in de `App.config` uit het Unit Test Project. Deze werkwijze is eveneens **NOT DONE**. Wanneer bij de studenten de juiste versie van Visual Studio 2013 en SQL Server 2012 optimaal werken, zullen we deze connecties aanpassen naar een volwaardige SQL-databank draaiend op een **echte** SQL-server.

```xml
<connectionStrings>
    <add name="DefaultConnection" connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=UniveristyCMSv4;Integrated Security=False;User ID=...;Password=...;Pooling=False" providerName="System.Data.SqlClient" />
  </connectionStrings>
```

###Migrations

Enkel toepasbaar indien we "Models First" aanpak toepassen. En dit doen we ook voor UniversityCMS.

> Tools \> NuGet Package Manager \> Package Manager Console

```
PM> Enable-Migrations -ProjectName UniversityCMS.Data

PM> Add-Migration -ProjectName UniversityCMS.Data "Naam van de migratie -StartupProjectName UniversityCMS.Web"

PM> Update-database -ProjectName UniversityCMS.Data -StartupProjectName UniversityCMS.Web -verbose

```

> **Voorbeeld van uitgevoerde migrations in de het project UniversityCMS.Data:**
>  
>  ![enter image description here](https://lh5.googleusercontent.com/-RQ6rN3tNClU/VClrhyvgyHI/AAAAAAAAAYs/JzeaWH4yKjg/s0/prjdbmigrations.PNG "prjdbmigrations.PNG")

***

> **Voorbeeld van uitgevoerde migrations in de databank:**
>  
>  ![enter image description here](https://lh5.googleusercontent.com/-ODozQ0tnlS8/VClrGL97aFI/AAAAAAAAAYg/3d5EkfPmBm0/s0/tdmigrations.PNG "tdmigrations.PNG")

***
> **Voorbeeld van aangemaakte tabellen in de databank onder de server explorer na migraties:**
>  
>  ![enter image description here](https://lh3.googleusercontent.com/-lWSnI8OWbAc/VClr9DahSLI/AAAAAAAAAZA/FlYbNc1n2A4/s0/dbserverexplorer.PNG "dbserverexplorer.PNG")

##Modellen mappen: Post, Category, Tag, Comment, ...

De workflow om gestructureerde webapplicaties aan te maken met MVC is relatief eenvoudig: 

1. Maak eerst het Model aan
2. Vervolgens map je het Model met de corresponderende Tabel
3. Tenslotte schrijven we enkele **Unit Tests** om deze *mapping* te testen.

### Post

Een **Post** is een Model om een blog-post te beschrijven. 

#### Model

Een Post bevat de volgende initiële eigenschappen:

- **Id**
De Id of identificatie van een categorie. Dit komt overeen met de primaire sleutel van een rij in de tabel *Posts* in de databank.
- **Title**  
De titel van een post.
- **Synopsis**  
De synopsis of samenvatting van een post.
- **Content** of **Body**  
De verhalende inhoud van een post. Kan tekst, afbeeldingen, video's e.d. bevatten.
- **Created**  
Een uniek moment waarin een post werd aangemaakt. Dit is meestal een **timestamp** dat gebaseerd kan zijn op een *concurrency token* of *rowversion*.
- **Updated**  
Datum en tijd waarop deze post gewijzigd is.
- **Deleted**  
Datum en tijd waarop deze post virtueel verwijderd werd. Dit is in **Laravel** gekend als een *soft-delete*.

####Mapping

```
...

namespace UniversityCMS.Data.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        #region Properties
        public virtual IDbSet<Post> Posts { get; set; }
        #endregion

        #region Constructor
        public ApplicationDbContext():base("DefaultConnection"){}
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
                throw new ArgumentNullException("modelBuilder");

            base.OnModelCreating(modelBuilder);

            //Map Post Model to Posts Table
            EntityTypeConfiguration<Post> efConfPost = modelBuilder.Entity<Post>().ToTable("Posts");
            efConfPost.HasKey((Post m) => m.Id);//Linq notation
            efConfPost.Property((Post m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfPost.Property((Post m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfPost.Property((Post m) => m.Title).IsRequired().HasMaxLength(256);
            efConfPost.Property((Post m) => m.Synopsis).IsRequired().HasMaxLength(1024);
            efConfPost.Property((Post m) => m.Content).IsRequired();
            efConfPost.Property((Post m) => m.Updated).IsOptional();
            efConfPost.Property((Post m) => m.Deleted).IsOptional();            
        }
        #endregion

        ...
    }
}
```

> **Enkele toelichtingen i.v.m. Mapping via fluent API**
>  
> - `ToTable(naam van de tabel)`  
Model wordt gemapt op een bepaalde, lees corresponderende tabel.
> - `HasKey(eigenschap van het model)`  
Een eigenschap van het model wordt aangeduid als primaire sleutel.
> - `IsOptional()`  
Veld is optioneel.
> - `IsRequired()`  
Veld is vereist.
> - `HaxMaxLength(string lengte)`  
Maximale lengte van de string in het veld. Positief geheel getal, bv. 256.
> - `HasDatabaseGeneratedOption(DatabaseGeneratedOption type)`  
Het veld heeft of bevat een optie waarbij de waarde van het veld gegenereerd wordt door de databank. Mogelijke typen:  `DatabaseGeneratedOption.Identity` (identiteit of primaire sleutel van een rij), `DatabaseGeneratedOption.Computed` (de waarde wordt berekend door de database, meestal een timestamp gebaseerd op concurrencytoken of rowversion), `DatabaseGeneratedOption.None` (geen generatie).


####Migration
```
PM> Add-Migration -ProjectName UniversityCMS.Data "Add Post Model"

PM> Update-database -ProjectName UniversityCMS.Data -StartupProjectName UniversityCMS.Web -verbose

```

####Unit test methoden

- `Create_Post`  
Aanmaak van een Post
- `Get_Post`  
Ophalen van een specifieke Post
- `Update_Post`  
Wijzigen/updaten van een Post
- `VirtualDelete_Post`  
Virtueel verwijderen van een Post
- `VirtualUnDelete_Post`  
Virtueel verwijderen van een Post ongedaan maken


```cs
		[TestMethod]
        public void Create_Post()
        {
            var post = new Post();
            post.Title = "IDF 2014: op weg naar het kleinste - Developers aan de bak bij Intel";
            post.Synopsis = "Half september vond in de Amerikaanse stad San Francisco naar goed gebruik het Intel Developer Forum plaats. Net als elke editie had ook de veertiende weer enkele thema's die centraal stonden. Intel legde tijdens IDF14 vooral de nadruk op het Internet of Things, waarbij de toekomst bol staat van de intelligente apparaten om je heen.";
            post.Content = "<p>Half september vond in de Amerikaanse stad San Francisco naar goed gebruik het Intel Developer Forum plaats. Net als elke editie had ook de veertiende weer enkele thema's die centraal stonden.</p>";

            _context.Posts.Add(post);
            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```

> **Resultaat in de database:**
>  
>  ![enter image description here](https://lh4.googleusercontent.com/-DbMfoqrPZro/VClqnWf5ZPI/AAAAAAAAAX8/S4gKcgNrkrw/s0/tdpost.PNG "tdpost.PNG")

```cs
		[TestMethod]
        public void Get_Post()
        {
            var post = _context.Posts.Find(1);

            Assert.IsNotNull(post);
        }
```

```cs
		[TestMethod]
        public void Update_Post()
        {
            var post = _context.Posts.Find(1);//get a specific Post (certain id)
            Assert.IsNotNull(post);

            post.Title = "IDF 2014: op weg naar het kleinste - Developers aan de bak bij Intel";

            _context.Posts.Attach(post);//attach must be used when we update or delete an entity
            _context.Entry(post).State = System.Data.Entity.EntityState.Modified;//set state of entity as modified

            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```

```cs
		[TestMethod]
        public void VirtualDelete_Post()
        {
            var post = _context.Posts.Find(1);//get a specific Post (certain id)
            Assert.IsNotNull(post);

            post.Deleted = DateTime.Now;

            _context.Posts.Attach(post);//attach must be used when we update/delete an entity
            _context.Entry(post).State = System.Data.Entity.EntityState.Modified;//set stae of entity as modified

            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```

```cs
		[TestMethod]
        public void VirtualUnDelete_Post()
        {
            var post = _context.Posts.Find(1);//get a specific Post (certain id)
            Assert.IsNotNull(post);

            post.Deleted = null;

            _context.Posts.Attach(post);//attach must be used when we update/delete an entity
            _context.Entry(post).State = System.Data.Entity.EntityState.Modified;//set stae of entity as modified

            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```

### Category

Een **Category** is een Model om een categorie te beschrijven. 

#### Model

Een Category bevat de volgende initiële eigenschappen:

- **Id**
De Id of identificatie van een categorie. Dit komt overeen met de primaire sleutel van een rij in de tabel *Categories* in de databank.
- **Name**  
De titel van een categorie.
- **Description**  
De synopsis of samenvatting van een categorie.
- **Created**  
Een uniek moment waarin een categorie werd aangemaakt. Dit is meestal een **timestamp** dat gebaseerd kan zijn op een *concurrency token* of *rowversion*.
- **Updated**  
Datum en tijd waarop deze categorie gewijzigd is.
- **Deleted**  
Datum en tijd waarop deze categorie virtueel verwijderd werd.

####Mapping

```
...

namespace UniversityCMS.Data.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        #region Properties
        public virtual IDbSet<Post> Posts { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<Tag> Tags { get; set; }
        #endregion

        #region Constructor
        public ApplicationDbContext():base("DefaultConnection"){}
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ...

            //Map Category Model to Category Table
            EntityTypeConfiguration<Category> efConfCategory = modelBuilder.Entity<Category>().ToTable("Categories");
            efConfCategory.HasKey((Category m) => m.Id);//Linq notation
            efConfCategory.Property((Category m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfCategory.Property((Category m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfCategory.Property((Category m) => m.Name).IsRequired().HasMaxLength(64);
            efConfCategory.Property((Category m) => m.Description).IsOptional().HasMaxLength(512);
            efConfCategory.Property((Category m) => m.Updated).IsOptional();
            efConfCategory.Property((Category m) => m.Deleted).IsOptional();
        }
        #endregion

        ...
    }
}

```

####Migration

```
PM> Add-Migration -ProjectName UniversityCMS.Data "Add Category Model"

PM> Update-database -ProjectName UniversityCMS.Data -StartupProjectName UniversityCMS.Web -verbose

```

####Unit test methoden

- `Create_Category`  
Aanmaak van een Category
- `Get_Category`  
Ophalen van een specifieke Category
- `Update_Category`  
Wijzigen/updaten van een Category
- `VirtualDelete_Category`  
Virtueel verwijderen van een Category
- `VirtualUnDelete_Category`  
Virtueel verwijderen van een Category ongedaan maken

```cs
		[TestMethod]
        public void Create_Category()
        {
            var category = new Category {
                               Name = "Web development",
                               Description = "Web design and Development for 633K"
                           };

            _context.Categories.Add(category);

            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```


> **Resultaat in de database:**
>  
>  ![enter image description here](https://lh4.googleusercontent.com/-gYo37ifrpII/VClqy2tjGHI/AAAAAAAAAYI/Yu3mtyhaAd8/s0/tdcategory.PNG "tdcategory.PNG")

```cs
		[TestMethod]
        public void Get_Category()
        {
            var category = _context.Categories.Find(1);

            Assert.IsNotNull(category);
        }
```

De andere methoden zijn gelijkaardig met deze uit de unit testing van een Post.

### Tag

Een **Tag** is een Model om een tag te beschrijven. 

#### Model

Een Tag bevat de volgende initiële eigenschappen:

- **Id**
De Id of identificatie van een tag. Dit komt overeen met de primaire sleutel van een rij in de tabel *Tags* in de databank.
- **Name**  
De titel van een tag.
- **Created**  
Een uniek moment waarin een tag werd aangemaakt. Dit is meestal een **timestamp** dat gebaseerd kan zijn op een *concurrency token* of *rowversion*.
- **Updated**  
Datum en tijd waarop deze tag gewijzigd is.
- **Deleted**  
Datum en tijd waarop deze tag virtueel verwijderd werd.

####Mapping

```
...

namespace UniversityCMS.Data.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        #region Properties
        ...
        public virtual IDbSet<Tag> Tags { get; set; }
        #endregion

        #region Constructor
        public ApplicationDbContext():base("DefaultConnection"){}
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ...

            //Map Tag Model to Tag Table
            EntityTypeConfiguration<Tag> efConfTag = modelBuilder.Entity<Tag>().ToTable("Tags");
            efConfTag.HasKey((Tag m) => m.Id);//Linq notation
            efConfTag.Property((Tag m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfTag.Property((Tag m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfTag.Property((Tag m) => m.Name).IsRequired().HasMaxLength(64);
            efConfTag.Property((Tag m) => m.Updated).IsOptional();
            efConfTag.Property((Tag m) => m.Deleted).IsOptional();
        }
        #endregion

        ...
    }
}

```

####Migration

```
PM> Add-Migration -ProjectName UniversityCMS.Data "Add Tag Model"

PM> Update-database -ProjectName UniversityCMS.Data -StartupProjectName UniversityCMS.Web -verbose

```

####Unit test methoden

- `Create_Tag`  
Aanmaak van een Tag
- `Get_Tag`  
Ophalen van een specifieke Tag
- `Update_Tag`  
Wijzigen/updaten van een Tag
- `VirtualDelete_Tag`  
Virtueel verwijderen van een Tag
- `VirtualUnDelete_Tag`  
Virtueel verwijderen van een Tag ongedaan maken

```cs
		[TestMethod]
        public void Create_Tag()
        {
            var tag = new Tag
            {
                Name = "Laravel"
            };

            _context.Tags.Add(tag);

            var result = _context.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
```

> **Resultaat in de database:**
>  
>  ![enter image description here](https://lh6.googleusercontent.com/-6JgmeAHr9aM/VClq6DDISSI/AAAAAAAAAYU/Ug31uZdeyEg/s0/tdtag.PNG "tdtag.PNG")

```cs
		[TestMethod]
        public void Get_Tag()
        {
            var tag = _context.Tags.Find(1);

            Assert.IsNotNull(tag);
        }
```

*[CRUD]: Create Read Update Delete
*[ORM]: Object Relational Mapping

###Relaties tussen Post, Category en Tag modellen

Een **Post** kan meerdere categorieën bevatten alsook meerdere tags. Een **Category** kan toegekend worden aan één of meerdere posts. Een **Tag** kan toegekend worden aan één of meerdere posts. Zo'n relaties zijn algemeen gekend als **veel-op-veel** relaties. Op databaseniveau worden deze relaties gerealiseerd d.m.v. een koppeltabel. Een koppeltabel bevat twee kolommen, namelijk de primaire sleutels van beide gekoppelde tabellen. De naamgeving van zo'n koppeltabel bevat de namen van de gekoppelde tabellen gescheiden door `_Has_`, bv.:  `Posts_Has_Categories`.

####Modellen

We moeten van een bepaalde post de categorieën en tags kunnen opvragen. Van een bepaalde categorie moeten we de posts kunnen opvragen die o.a. deze specifieke categorie bevatten. Van een bepaalde tag moeten we de posts kunnen opvragen die o.a. deze specifieke tag bevatten. Dit betekent dat we de modellen moeten aanpassen, d.w.z. met eigenschappen op properties die deze functionaliteiten bevatten. Zo'n eigenschappen noemen we **Navigation Properties**.

```cs
	...
	public class Post
    {
        ...

        #region Navigation Properties
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        #endregion
    }
```

```cs
	...
	public class Category
    {
        ...

        #region Navigation Properties
        public virtual ICollection<Post> Posts { get; set; }
        #endregion
    }
```

```cs
	...
	public class Tag
    {
        ...

        #region Navigation Properties
        public virtual ICollection<Post> Posts { get; set; }
        #endregion
    }
```

####Mapping

```cs
	...
    public class ApplicationDbContext : DbContext
    {
        ...

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
                throw new ArgumentNullException("modelBuilder");

            base.OnModelCreating(modelBuilder);

            //Map Post Model to Posts Table
            ...
            efConfPost
                .HasMany((Post m) => m.Categories)
                .WithMany((Category c) => c.Posts)
                .Map(mc =>
                {
                    mc.ToTable("Post_Has_Categories");
                    mc.MapLeftKey("PostId");
                    mc.MapRightKey("CategoryId");
                });
            efConfPost
                .HasMany((Post m) => m.Tags)
                .WithMany((Tag t) => t.Posts)
                .Map(mc =>
                {
                    mc.ToTable("Post_Has_Tags");
                    mc.MapLeftKey("PostId");
                    mc.MapRightKey("TagId");
                });
        }
        #endregion

        ...
    }
    ...
```

####Migrations

####Unit test methoden

##ASP.NET Identity2

###Modellen

####ApplicationUser

```
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace UniversityCMS.Model
{
    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        #region Extra Properties
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Generate my own Identity
        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = manager.CreateIdentity<ApplicationUser, int>(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        #endregion
    }
}
```

####ApplicationUserLogin

```cs
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityCMS.Model
{
    public class ApplicationUserLogin : IdentityUserLogin<int>
    {
    }
}
```

####ApplicationUserRole

```cs
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityCMS.Model
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
    }
}
```

####ApplicationUserClaim

```cs
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityCMS.Model
{
    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
    }
}
```

####ApplicationRole

```cs
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace UniversityCMS.Model
{
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>
    {
        #region Extra Properties
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion       
    }
}
```

###Uitbreiding van de ApplicationDbContext klasse met Identity2

```cs
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversityCMS.Data.Initializer;
using UniversityCMS.Model;

namespace UniversityCMS.Data.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,int,ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        #region PROPERTIES
        public virtual IDbSet<Post> Posts { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<Tag> Tags { get; set; }
        public virtual IDbSet<Comment> Comments { get; set; }
        #endregion

        ...
    }
}
```

###Migration


```
PM> Add-Migration -ProjectName UniversityCMS.Data "Add Identity2 Models"

PM> Update-database -ProjectName UniversityCMS.Data -StartupProjectName UniversityCMS.Web -verbose

```

##Backoffice

###Areas

```
context.MapRoute(
                "Backoffice_default",
                "Backoffice/{controller}/{action}/{id}",
                defaults:new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces:new[]{"UniversityCMS.Web.Areas.Backoffice.Controllers"}).DataTokens["UseNamespaceFallback"] = false;
```


**Aanpassen andere routes binnen `RouteConfig.cs` onder de folder `App_Start`.**
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UniversityCMS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "UniversityCMS.Web.Controllers" }).DataTokens["UseNamespaceFallback"] = false;
        }
    }
}
```

##Webserver

Om webapplicaties uit te voeren kunnen we gebruik maken van:

- Ingebouwde webserver
- Internet Information Server (IIS)
- Azure Cloud Services

###Installatie van Windows onderdelen

![enter image description here](https://lh6.googleusercontent.com/-2H5xh_fJkxE/VDY0E1jcprI/AAAAAAAAAjQ/3j1p74qWRkg/s0/windows_server_instellingen.PNG "windows_server_instellingen.PNG")

###IIS

Via de **Microsoft Management Console** kunnen we een aantal services raadplegen en beheren, na toevoeging in deze MMC, via een interface. Deze MMC is uit te voeren via de executable `mmc.exe`.  We kunnen op Windows zoeken naar dt bestand, maar we kunnen dit commando ook gewoon ingeven via de *Command Prompt* of *PowerShell*. Standaard staan zijn er geen modules toegevoegd binnen MMC, dit moeten we zelf manueel uitvoeren.

> **MMC zoeken op windows:**
>  
>  ![enter image description here](https://lh5.googleusercontent.com/-D6iuIuAAFSY/VCmIQjqpjUI/AAAAAAAAAZw/3L9YSF1toUw/s0/mmc.png "mmc.png")

Om een module toe te voegen kunnen we hetvolgende uitvoeren:

> Bestand \> Module Toevoegen/Verwijderen

We selecteren vervolgens **Beheer Internet Information Server** en klikken op de knop toevoegen. Op diezelfde manier voegen we een aantal andere server-gerelateerde modules toe, die van belang zijn in webdevelopment, namelijk:

- **SQL Server Configuration Manager**  
Configuratie van de lokale SQL Server.
- **Certificaten**  
Beheer van certificaten op het lokaal toestel.
- **Logboeken**  
Logging van applicaties, systeem, security, ... .
- **Services**  
Beheer van geïnstalleerde services op het lokaal toestel.
- **Indexing-services**  
Indexering van inhoud van webapplicaties.
- **Lokale gebruikers en groepen**  
Beheer van lokale gebruikers en groepen

Het is mogelijk dat niet alle bovenstaande modules beschikbaar zijn op jullie systeem. Geen nood! We hebben vooral de **IIS-beheer module** nodig!

> **Geïnstalleerde MMC-modules:**
>  
>  ![enter image description here](https://lh4.googleusercontent.com/-K13GRwwDeS8/VCmKaoEgFOI/AAAAAAAAAaA/cPILpejhcLw/s0/iismodules.PNG "iismodules.PNG")

We bewaren deze instellingen in een nieuw bestand **web.mmc** dat je ergens op je schijf bewaard. Door volgende keer op dit bestand te klikken, zal MMC alle toegevoegde modules bevatten.

Klikken we op de module **IIS-beheer** dan krijgen we het dashboard van IIS te zien.

> **Dashboard IIS-beheer:**
>  
>  ![Dashboard IIS-beheer](https://lh6.googleusercontent.com/-5Y74xoLECZg/VCmM2s3CVMI/AAAAAAAAAaQ/s2pDD2y9XoE/s0/iisdashboard.PNG "iisdashboard.PNG")

Om extra IIS-instellingen, applicaties en dergelijke eenvoudig te installeren maken we best gebruik van de **Web Platform Installer**, te downloaden via <http://www.microsoft.com/web/downloads/platform.aspx>.

> **Download Web Platform Installer:**
>  
>  ![enter image description here](https://lh4.googleusercontent.com/-oMr2p8XP4ak/VDrHz0hOEAI/AAAAAAAAAkg/wFxvPAnDKME/s0/webplatforminstaller.PNG "webplatforminstaller.PNG")

Na installatie is deze nieuwe module **Web Platform Installer** beschikbaar via IIS-beheer. Noodzakelijke installaties:

- Server
	- Application Request Routing 3.0
	- External Cache 1.0
	- URL Rewrite 2.0
	- IIS 7.5 Express
	- Web Deploy 3.5
- Frameworks
	- Microsoft .Net Framework 4.5.2
	- Windows Powershell 3.0

*[IIS]: Internet Information Server
*[MMC]: Microsoft Management Console

##Bibliografie

> **DbContext**
>
> - <http://msdn.microsoft.com/nl-be/data/jj729737.aspx>
> - <http://www.asp.net/mvc/tutorials/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application>
> - <http://www.dotnetcurry.com/showarticle.aspx?ID=938>
> - <http://www.connectionstrings.com/sql-server/>
>     
> **Entity Framework**
>   
>   - <http://msdn.microsoft.com/nl-be/data/ef.aspx>
>   - <http://www.entityframeworktutorial.net/>
> 
> **Unit Test**
> 
> - <http://blogs.msdn.com/b/visualstudioalm/archive/2012/11/09/how-to-manage-unit-tests-in-visual-studio-2012-update-1-part-1-using-traits-in-the-unit-test-explorer.aspx>
> - <http://www.codewrecks.com/blog/index.php/2013/03/14/new-unit-test-functionality-in-vs2012-update-2-test-playlist/>
> - <http://msdn.microsoft.com/en-us/data/dn314429.aspx>
> - <http://www.nuget.org/packages/Moq/>
> - <http://gaui.is/how-to-mock-the-datacontext-entity-framework/>
> - <http://msdn.microsoft.com/en-us/data/dn314431.aspx>
> 
> **MVC**
> 
> - <http://www.codeproject.com/Articles/476967/WhatplusisplusViewData-cplusViewBagplusandplusTem>
> 
> 
> **Documentatie generatie**
>  
>  - <http://blogs.msdn.com/b/msgulfcommunity/archive/2014/04/22/creating-documentation-in-c-using-visual-studio-and-sandcastle.aspx>
>  - <http://www.simple-talk.com/dotnet/.net-tools/taming-sandcastle-a-.net-programmers-guide-to-documenting-your-code>
>  - <http://www.ewoodruff.us/shfbdocs/html/bd1ddb51-1c4f-434f-bb1a-ce2135d3a909.htm>
>  - <http://submain.com/products/ghostdoc.aspx>

