﻿WDAD-III 2014-15
===============

***

[TOC]

***

|Info||
|----|----|
|Lector|**Philippe De Pauw - Waterschoot**|
|Aantal lestijden|**4CU** (= 6 uren)|
|Aantal studiepunten|**7STP** (=210 uren)|
|Aantal uren werken aan project buiten de colleges|210-6*12= 138 uren = **minimaal 17,25 dagen full time**|
|ECTS|[ECTS Fiche](http://www.arteveldehogeschool.be/ects/ahsownapp/ects/ECTSFiche.aspx?olodID=42073)|

Inhoud
--------
- Wat voorafging
- User Experience Design
- Geautomatiseerde workflows opzetten en toepassen.
- Front-end framework(s).
- JavaScript-Framework(s).
- Mobile Development Framework.
- ASP.NET via een Enterprise Level-framework: OOP, MVC, RESTful, Templating, ORM, Unit Testing, Functional Testing.
- Database Modeling.
- Opdracht(en)

Evaluatie
-----------
- 1e examenkans
	- Werkstuk: 70%
	- Mondeling examen + mondelinge verdediging van werkstuk 30%
	- Groepswerk: Ja, groepen van maximaal 2 studenten.
- 2e examenkans
	- Werkstuk: 70%
	- Mondeling examen + mondelinge verdediging van werkstuk 30%
	- Groepswerk: Neen, Individueel! Startend van de eindtoestand van de 1ste examenkans + extra features te realiseren om de competenties te verwerven.

Studenten & groepsindeling
---------------------------------
- Beeckman Pieter-Jan
- Boute Ben
- Carlier Glen
- Debbaut Gennadi
- Decorte Bart
- Denys Nick
- De Bock Vincent
- De Deyne Sebastian
- De Marez-Desmet Lothar
- Geerolf Rutger
- Goetynck Gertjan
- Meert Sander
- Mouton Pieter
- Schroé Stephanie
- Simoen Enzo
- Van Biesen Arno
- Van Damme Florian
- Van den Broecke Robby
- Vanderkimpen Stijn
- Vanhecke Nicolas
- Vanmelkebeke Florian
- Moulin Manon

**Groepsvorming**

|Groep|Studenten|
|-----|---------|
|Groep1|De Marez-Desmet Lothar, Vincent De Bock|
|Groep2|De Deyne Sebastian, Gennadi Debbaut|
|Groep3|Mouton Pieter, Carlier Glen, Schroé Stephanie (Super App)|
|Groep4|Van Biesen Arno, Vanmelkebeke Florian|
|Groep5|Van den Broecke Robby, Van Damme Florian|
|Groep6|Beeckman Pieter-Jan, Simoen Enzo|
|Groep7|Denys Nick, Meert Sander|
|Groep8|Vanderkimpen Stijn, Vanhecke Nicolas|
|Groep9|Goetynck Gertjan|
|Groep10|Decorte Bart|
|Groep11|Boute Ben, Geerolf Rutger|
|Groep12|Moulin Manon|


Opdracht: Digitaal schoolportaal
---------------------------------------

|Info||
|-----|---|	
|Werktitel|Digitaal schoolportaal.|
|Subtitel|Digitaal  schoolportaal voor het secundair onderwijs.|
|Milestone 1|**13-11-2014** Presentatie tussentijds productiedossier (Briefing, analyse, persona's, Moodboard, Wireframes, Sitemap|
|Milestone 2|**11-12-2014** Presentatie Visuele Designs en tussentijds prototype.
|Deadline|**04/01/2015 22u** - Opleveren opdracht|

###Tussentijds productiedossier (milestone 1)
- Moet geschreven worden in Markdown (Stackedit.io + Google Sync)
- Briefing
- Analyse
- Technische specificaties
- Functionele specificaties
- Persona's (+ scenario)
- Moodboard
- Sitemap
- Wireframes

> Presentatie 10min/groep

###Technische specificaties

- Verplichte Solution Structuur (SchoolAgenda)
	- Data.Pattern
	- SchoolAgenda.Data
	- SchoolAgenda.Data.Test 
	- SchoolAgenda.Model
	- SchoolAgenda.Web
	- SchoolAgenda.Web.Test (optioneel)  
	- SchoolAgenda.WebAPI
	- SchoolAgenda.WebAPI.Test
	- SchoolAgenda.WebFEClient (AngularJS)  
	- SchoolAgenda.WebFEClient.Test (optioneel)  
	
- Versiebeheer  
Maak een nieuw Git Repository voor je project aan op Bitbucket. Code worden tussen groepen niet gedeeld! Deel het project met de docent(en).

- **Data.Pattern** (UnitOfWork + Repository)  
Generieke UnitOfWork en Repository. Verplicht gebruik in Unit Test Projecten, MVC5+ Webapps en WebAPI's voor de opdracht SchoolAgenda.
- **Data** (datalaag voor alle CRUD-operaties)  
DbContext, Seeding, en Stores. 
- **Data.Test** (Unit testing van de voorgaande datalaag)  
- **Model** (laag voor de modellen en de **view**modellen)
- **Web** (frontoffice en backoffice)
**Responsive "mobile-first" MVC5+ webapplicatie!**
Geen connectie met de WebAPI!
HTML5, CSS3, JavaScript & jQuery
Twitter Bootstrap of Foundation Zurb!
Maak zoveel mogelijk gebruik van herbruikbare componenten, lees **Partial Views**!
- **WebFEClient** (pure frontend)  
**Webapplicatie moet de look-and-feel van een native applicatie! **
HTML5, CSS3 en JavaScript
AngularJS
Twitter Bootstrap, Foundation Zurb, Ionic Framework of andere... (toelating vragen aan de docent)
jQuery, underscore.js, lodash.js, ... (toelating vragen aan de docent)
localstorage of IndexedDB (tijdelijke data)

- Tips
	- <http://www.pttrns.com/>
	- <http://www.mobile-patterns.com/>

###Opleveren

- Print
	- Productiedossier
	- Poster A2
- Digitaal
	- ISO-bestand
		- README.md (Markdown-bestand met informatie van de groepsleden en groepsnummer)
		- documents (folder)
			- dossier.md
			- dossier.pdf
			- poster.pdf
			- poster.png
		- screenshots (folder)
		- screencasts (folder)
- Online
	- Bitbucket
		- Naam repository: **wdadiii_201415_schoolagenda_3MMPproDEV_groep{1...n}**

Naamgeving ISO-bestand: **wdadiii_201415_schoolagenda_3MMPproDEV_groep{1...n}.iso**

###Persona's

- student
- teacher
- classtitular
- secretary
- principal
- parent
- schoolcoordinator
- administrator

###Features en begrippen

Een **Schooljaar (schoolyears)**:

- naam, code, omschrijving, ...

Een **Opleiding**:

- naam, code, omschrijving, ...
- toegekend aan een schooljaar

Een **Klas (classes)**:

- naam, code, omschrijving, ...
- bevat studenten
- bevat een titularis
- toegekend aan een opleiding
- (gekoppelde studenten, vakken, ...)

Een **Vak (courses)**:

- naam, code, omschrijving, ...
- aantal lesblokken/tijden
- toegekend aan een opleiding
- (gekoppelde leraren, klassen, studenten, ...)

Een **Leerling (students)**:

- voornaam, familienaam, geslacht, geboortedatum, ...
- studentennummer, account, email
- (gekoppelde ouders, vakken, ...)

Een **Taak (tasks)**:

- Een taak kan opgelegd worden voor een bepaald vak met als einddatum één van de volgende lessen (van dat bepaald vak)
- Punten kunnen door de leraar toegekend worden na evaluatie van de taak
- Een taak behoort tot het dagelijkse werk!

Een **Toets (tests)**:

- Een toets wordt afgenomen tijdens één van de lessen.
- Vooraf wordt door de leraar aangekondigd wat de leerstof is voor deze toets!
- Punten kunnen door de leraar toegekend worden na evaluatie van deze toets
- Een toets behoort tot het dagelijkse werk!

Een **Ingeroosterde les (lessons)**:

- bepaald vak
- gegeven door een bepaalde leraar
- aan een bepaalde klas
- in een bepaald lokaal
- op een bepaald tijdsmoment tot een bepaald tijdsmoment
- bevat specifieke lesinhoud

Een **Ingeroosterde examen (examens)**:

- bepaald vak
- gegeven door een bepaalde leraar
- aan een bepaalde klas
- in een bepaald lokaal
- op een bepaald tijdsmoment tot een bepaald tijdsmoment
- bevat specifieke inhoud voor het examen

Een **Leerling (students)**:

- kan bekijken:
	- alle roosters die hen aanbelangen
	- de blog bekijken van de vakken die hen aanbelangen en ook van de klas
	- de lesinhouden van zijn/haar vakken
	- resultaten op taken, toetsen en examens
- kan berichten sturen naar vakdocenten en klastitularis
- kan berichten beantwoorden
- moet zijn/haar agenda wekelijks digitaal laten ondertekenen door zijn/haar ouders
- moet zijn/haar rapporten digitaal laten ondertekenen door zijn/haar ouders

Een **Ouder (parents)**:

- kan alles bekijken van hun ingeschreven kind(eren)
- moet de agenda (van hun kind(eren) wekelijks digitaal ondertekenen
- moet de rapporten  (van hun kind(eren)  digitaal ondertekenen door zijn/haar ouders

Een **Leraar beheert (teachers)**:

- de lesinhouden van zijn/haar vakken
- punteningave voor taken, toetsen en examens
- opmerkingen toevoegen na een taak, toets en/of examen
- de blog van een vak
- kan berichten sturen naar docenten en andere medewerkers
- kan berichten beantwoorden

Een **Klastitulairs beheert (classtitulars)**:

- de blog van een klas
- de problemen binnen een klas
- de problemen met leraren die les geven aan die specifieke klas
- kan berichten sturen naar studenten, docenten en andere medewerkers
- kan berichten beantwoorden

Een **Schoolcoordinator beheert (secretaries)**:

- de leraren
- de klastitularissen
- de lokalen
- de lesblokken
- de opleidingen
- de roosters van de secundaire school (vakantiedagen, evenementen, lessen, examens, ...)
- kan berichten sturen naar docenten en andere medewerkers
- kan berichten beantwoorden
- (kan alle andere data bekijken)

Een **Secretariaatsmedewerker beheert**:

- de studenten
- de blog van de opleiding
- de blog van een klas
- de afwezigheden van leraren
- de afwezigheden van studenten

Een **Opleidingsdirecteur (principal)**:

- kan alle data bekijken
- kan berichten sturen naar docenten en andere medewerkers
- kan berichten beantwoorden

Een **Administrator kan alles beheren!**

###Bibliografie

- <http://www.sui.be/?p=onzeschool/wieiswie>
- <http://www.school-agenda.com/>
- <http://www.smartschool.be/smartschool/digitale-schoolagenda/>
- <https://www.youtube.com/watch?v=aXTEKihVf14>