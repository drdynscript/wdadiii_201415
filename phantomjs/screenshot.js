var page = require('webpage').create(),
    system = require('system');

if(system.args.length === 1){
    console.log('Usage: loadpageadv.js <some URL>');
    phantom.exit();
}

var url = system.args[1];

page.open(url, function(p){
    if(p !== 'success'){
        console.log('Failed to load the webpage!')
    }else{
        var t = new Date().getTime();

        var title = page.evaluate(function(){
           return document.title;
        });

        var scn = title.split(' ')[0] + "_" + t + ".png";

        page.render(scn);
    }
    phantom.exit();
});