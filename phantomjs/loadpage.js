var page = require('webpage').create();

var start = Date.now();

page.open('http://127.0.0.1', function(p){
    if(p !== 'success'){
        console.log('Failed to load the webpage!')
    }else{
        var end = Date.now();
        var diff = end - start;

        console.log('Time to load the page: ' + diff);
    }
    phantom.exit();
});